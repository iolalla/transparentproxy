# TransparentProxy

https://medium.com/@mlowicki/http-s-proxy-in-golang-in-less-than-100-lines-of-code-6a51c2f2c38c

Simple use case : You need if a transparent https proxy, but you neeed to see the URLS and headers, Why? To configure proxies or firewalls.

Generate the certs with certs.sh
compile tp.go:
$go build tp.go
$./tp
OUTPUT:
2020/06/17 11:29:01 URL:google.com:443
2020/06/17 11:29:01 HEADER Name:User-Agent Value: curl/7.64.0
2020/06/17 11:29:01 HEADER Name:Proxy-Connection Value: Keep-Alive
$ curl -Lv --proxy https://localhost:8888 --proxy-cacert server.pem https://google.com/?l=es
